package com.example.dell.cameratestsurface;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.klinker.android.simple_videoview.SimpleVideoView;
import com.sprylab.android.widget.TextureVideoView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 9/06/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private Activity activity;
    private ArrayList<String> pics;
    private LayoutInflater inflater;
    private CheckBox checkBox;
    private String filePath;
    //private  VideoView mVideoView;
    //private SimpleVideoView mVideoView;
    private TextureVideoView mVideoView;
    private boolean blnIsClicked = true;
    private  String file;
    private List<String> tempList = new ArrayList<>();

    public ViewPagerAdapter(Activity activity, ArrayList<String> pics) {
        this.activity = activity;
        this.pics = pics;
    }

    @Override
    public int getCount() {
        return pics.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View itemView = inflater.inflate(R.layout.viewpager_item, container, false);



        final ImageView img = (ImageView) itemView.findViewById(R.id.imagePager);
        mVideoView = (TextureVideoView) itemView.findViewById(R.id.video_view);
        final ImageView ivPLayButton = (ImageView) itemView.findViewById(R.id.ivPlayButton);
        //checkBox = (CheckBox) itemView.findViewById(R.id.cbSharePicture);
        //File file = new File(Environment.getExternalStorageDirectory() + "hagelschade/Dirk de Poeck/" + pics[position]);
        if( pics.size() > 0){
            file = pics.get(position);
           // tempList.add(position,file);
        }else{
            Intent intent = new Intent(activity, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Removes other Activities from stack
            activity.startActivity(intent);
        }


        if(file.contains("VID")){

            //mVideoView.setVisibility(View.GONE);
            ivPLayButton.setVisibility(View.VISIBLE);
            img.setVisibility(View.VISIBLE);
            File fFile = new File(file);
            Uri imageUri = Uri.fromFile(fFile);
            //img.setImageURI(imageUri);
            Picasso.with(activity).load("file://" + fFile).into(img);

        }else{
            img.setVisibility(View.VISIBLE);
           // mVideoView.setVisibility(View.GONE);
            ivPLayButton.setVisibility(View.GONE);
            Picasso.with(activity).load("file://" + file).into(img);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Toast.makeText(activity, "position = " + position, Toast.LENGTH_LONG).show();
                if(file.contains("VID")) {
                    String videoPath = file.replace("jpg", "mp4");
                    Intent intent = new Intent(activity, VideoViewActivity.class);
                    intent.putExtra("path", videoPath);
                    activity.startActivity(intent);
                } */
               /* if(blnIsClicked){
                    if(file.contains("VID")){
                      //  Toast.makeText(activity, "Geklikt", Toast.LENGTH_LONG).show();
                        mVideoView.setVisibility(View.VISIBLE);
                        ivPLayButton.setVisibility(View.GONE);
                        img.setVisibility(View.GONE);
                        //String path  = tempList.get(position);
                        String videoPath = file.replace("jpg","mp4");

                        Uri uri = Uri.parse(videoPath);

                        mVideoView.setMediaController(new MediaController(activity));
                        mVideoView.setVideoURI(uri);
                        mVideoView.requestFocus();
                        mVideoView.setMediaController(null);
                        mVideoView.start();
                        blnIsClicked = false;
                        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mVideoView.setVisibility(View.GONE);
                                ivPLayButton.setVisibility(View.VISIBLE);
                                img.setVisibility(View.VISIBLE);
                               // String path  = tempList.get(position);
                                File fFile = new File(file);
                                Uri imageUri = Uri.fromFile(fFile);
                                img.setImageURI(imageUri);
                                blnIsClicked = true;
                            }
                        });

                    }
                }else{
                    mVideoView.setVisibility(View.GONE);
                    ivPLayButton.setVisibility(View.VISIBLE);
                    img.setVisibility(View.VISIBLE);
                   // String path  = tempList.get(position);
                    File fFile = new File(file);
                    Uri imageUri = Uri.fromFile(fFile);
                    img.setImageURI(imageUri);
                    blnIsClicked = true;
                } */
            }
        });
        //String file = Environment.getExternalStorageDirectory() + "hagelschade/Dirk de Poeck/" + pics[position];
        //Glide.with(activity).from().into(img);
        //String file = "file://" + pics[position];

        //Uri uri = Uri.fromFile(file);
        //filePath = FilePath.getPath(activity, uri);
        //Picasso.with(activity).load("file://" + filePath).into(img);

                //Glide.with(context).from(file).into(imageView);

        /*ImageView img = (ImageView) itemView.findViewById(R.id.imagePager);
        int photoId = activity.getResources().getIdentifier(pics[position], "drawable", activity.getPackageName());
        img.setImageResource(photoId); */

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewGroup)container).removeView((View)object);
    }
}
