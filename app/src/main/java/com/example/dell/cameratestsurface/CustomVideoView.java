package com.example.dell.cameratestsurface;

import android.content.Context;
import android.widget.VideoView;

/**
 * Created by Dell on 19/07/2017.
 */

public class CustomVideoView extends VideoView {
    public CustomVideoView(Context context) {
        super(context);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        getHolder().setSizeFromLayout();
    }
}
