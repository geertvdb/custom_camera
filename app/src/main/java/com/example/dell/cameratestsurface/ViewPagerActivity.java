package com.example.dell.cameratestsurface;

import android.content.Intent;
import android.media.ExifInterface;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.dell.cameratestsurface.R.styleable.View;

public class ViewPagerActivity extends AppCompatActivity {

    ArrayList<String> lstList = new ArrayList<>();
    private ViewPager viewPager;
    private ArrayList<String> lstImagePath;
    private String rotate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pager);


        viewPager = (ViewPager) findViewById(R.id.pager);

        Intent intent = getIntent();
        String path = intent.getStringExtra("path");
        lstImagePath = intent.getStringArrayListExtra("list");
        if(path != null || path != ""){
            lstList.add(path);
            ViewPagerAdapter adapter = new ViewPagerAdapter(this,lstList );
            viewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        if(lstImagePath != null){
            ViewPagerAdapter adapter = new ViewPagerAdapter(this,lstImagePath );
            //int id = viewPager.getId();
           // Toast.makeText(this, "pagerID = " + " " + id, Toast.LENGTH_LONG).show();
            viewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

    }

    public void imageClick(View v) {
        int id = viewPager.getCurrentItem();
        //Toast.makeText(this, "current Item = " + " " + id, Toast.LENGTH_LONG).show();
        String path = lstImagePath.get(id);
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        String pat = FilePath.getPath(ViewPagerActivity.this, uri);
        rotate = getOrientation(pat);
        String videoPath = "";
        if (path != null){
            videoPath = path.replace("jpg","mp4");
            Intent intent = new Intent(ViewPagerActivity.this, VideoViewActivity.class);
            intent.putExtra("path", videoPath);
            intent.putExtra("orientation", rotate);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Path is leeg", Toast.LENGTH_LONG).show();
        }


    }

    private String getOrientation(String path) {
        rotate = "0";
        //String path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/20170525_132621.jpg";
        //String path = pat;  // /sdcard/emulated/0/ ....
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ExifInterface exif = new ExifInterface(Environment.getExternalStorageDirectory() + "/DCIM/Camera/20161116_120542.jpg");
        String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
        if (!TextUtils.isEmpty(rotationAmount)) {
            int rotationParam = Integer.parseInt(rotationAmount);
            switch (rotationParam) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = "0";
                    //Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = "90";
                    // Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = "180";
                    // Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = "270";
                    //  Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
            }
        }
        return rotate;
    }
}
