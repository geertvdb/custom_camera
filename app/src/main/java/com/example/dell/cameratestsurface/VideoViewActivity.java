package com.example.dell.cameratestsurface;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.klinker.android.simple_videoview.SimpleVideoView;
import com.sprylab.android.widget.TextureVideoView;

public class VideoViewActivity extends AppCompatActivity {

    private VideoView mVideoView;
   // private CustomVideoView mVideoView;
    private RelativeLayout relativeLayout;
    private boolean blnPauze = true;
    private SimpleVideoView simpleVideoView;
    private TextureVideoView textureVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
              //  WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_video_view);

        //mVideoView = (VideoView) findViewById(R.id.video_view);
        //simpleVideoView = (SimpleVideoView) findViewById(R.id.video_view);
        textureVideoView = (TextureVideoView) findViewById(R.id.video_view);
        relativeLayout = (RelativeLayout) findViewById(R.id.rlVideoView);

        Intent intent = getIntent();
        String path = intent.getStringExtra("path");
        String orientation = intent.getStringExtra("orientation");

        final Uri uri = Uri.parse(path);

      //  simpleVideoView.setRotation(Float.parseFloat(orientation));
      //  simpleVideoView.start(uri);

        textureVideoView.setVideoPath(path);
        textureVideoView.setMediaController(new MediaController(this));
        textureVideoView.setRotation(Float.parseFloat(orientation));
        textureVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final MediaPlayer mp) {
                textureVideoView.start();
            }
        });

        textureVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });



        //mVideoView.setMediaController(new MediaController(this));
     /*   mVideoView.setVideoURI(uri);
        mVideoView.requestFocus();
        mVideoView.setMediaController(null);
        //relativeLayout.setRotation(Float.parseFloat(orientation));
        mVideoView.setRotation(Float.parseFloat(orientation));
        mVideoView.start();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });

        mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mVideoView.stopPlayback();
                //finish();
            }
        }); */


     // Deze code om bij klik te pauzeren en starten, maar nu gebuik makend van ingebouwde controls
     /*   relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textureVideoView.isPlaying() && textureVideoView != null){
                   // simpleVideoView.pause();
                    textureVideoView.pause();
                    blnPauze = false;
                    Toast.makeText(VideoViewActivity.this, "PAUZE", Toast.LENGTH_SHORT).show();
                }else{
                    //simpleVideoView.start(uri);
                    textureVideoView.start();
                    blnPauze = true;
                    Toast.makeText(VideoViewActivity.this, "START", Toast.LENGTH_SHORT).show();
                }
            }
        });  */

       relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                finish();
                return true;
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
               // WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onStop() {
        super.onStop();
        //simpleVideoView.release();
        //simpleVideoView = null;
        textureVideoView.stopPlayback();
        textureVideoView = null;
    }
}
