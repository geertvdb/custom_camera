package com.example.dell.cameratestsurface;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.opengl.GLDebugHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 11/03/2017.
 */

public class CustomListAdapter extends ArrayAdapter<String>{

    ArrayList<String> dataList = new ArrayList<String>();
    Context context;
    private Bitmap bitmap;
    private String rotate;

    public CustomListAdapter(Context context, ArrayList<String> dataList) {
        super(context, R.layout.list_item, dataList);
        this.dataList = dataList;
        this.context=context;

    }

    static class LayoutHandler {
        ImageView imageView;
        ImageView ivIsVideo;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    private int lastPosition = -1;

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View row;
        final LayoutHandler layoutHandler;
        row = convertView;

        if(row == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            row = layoutInflater.inflate(R.layout.list_item, parent, false);
            layoutHandler = new LayoutHandler();
            layoutHandler.imageView = (ImageView) row.findViewById(R.id.ivPreview);
            layoutHandler.ivIsVideo = (ImageView) row.findViewById(R.id.ivIsVideo);
            row.setTag(layoutHandler);
        }else {
            layoutHandler = (LayoutHandler) row.getTag();
        }

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        row.startAnimation(animation);
        lastPosition = position;

        String imagePath = dataList.get(position); // return elk OBJECT van de list
        File file = new File(imagePath);
        if(imagePath.contains("VID")){
            layoutHandler.ivIsVideo.setVisibility(View.VISIBLE);
        }else{
            layoutHandler.ivIsVideo.setVisibility(View.GONE);
        }

        //Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        //layoutHandler.imageView.setImageBitmap(bitmap);
        Uri uri = Uri.fromFile(file);
        String pat = FilePath.getPath(context, uri);
        rotate = getOrientation(pat);
        //layoutHandler.imageView.setRotation(angle);
        //Picasso.with(context).load(file).rotate(Integer.valueOf(rotate)).placeholder(R.drawable.progress).into(layoutHandler.imageView);
        Glide.with(context).load(uri).into(layoutHandler.imageView);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = dataList.get(position);
                if(path.contains("VID")){
                    File file = new File(path);
                    Uri uri = Uri.fromFile(file);
                    String pat = FilePath.getPath(context, uri);
                    String rotate = getOrientation(pat);
                    String videoPath = path.replace("jpg","mp4");
                    Intent intent = new Intent(context, VideoViewActivity.class);
                    intent.putExtra("orientation", rotate);
                    intent.putExtra("path", videoPath);
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, ViewPagerActivity.class);
                    intent.putExtra("path", dataList.get(position));
                    context.startActivity(intent);
                }

            }
        });

        row.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(context, ViewPagerActivity.class);
                intent.putStringArrayListExtra("list", dataList);
                context.startActivity(intent);
                return true;
            }
        });

        return row;
    }

    private String getOrientation(String path) {
        rotate = "0";
        //String path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/20170525_132621.jpg";
        //String path = pat;  // /sdcard/emulated/0/ ....
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ExifInterface exif = new ExifInterface(Environment.getExternalStorageDirectory() + "/DCIM/Camera/20161116_120542.jpg");
        String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
        if (!TextUtils.isEmpty(rotationAmount)) {
            int rotationParam = Integer.parseInt(rotationAmount);
            switch (rotationParam) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = "0";
                    //Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = "90";
                   // Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = "180";
                   // Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = "270";
                  //  Toast.makeText(context, "rotate = " + rotate, Toast.LENGTH_LONG).show();
            }
        }
        return rotate;
    }
}
